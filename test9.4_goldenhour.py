import cv2
import numpy as np

#OPEN VIDEO
video = cv2.VideoCapture(0)

while True:
    ret, frame = video.read()
    if not ret:
        break

    #CONVERT TO HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    #ISOLATE CHANNELS
    h, s, v, = cv2.split(hsv)

    #ADJUST SATURATION LEVELS
    s = cv2.multiply(s, 1.45)

    #ADJUST EXPOSURE LEVELS
    v = cv2.multiply(v, 0.8) #DECREASED BY 90%

    #ADJUST CONTRAST LEVELS
    s = cv2.multiply(s, 1.2)
    v = cv2.multiply(v, 1.2)
    adjusted_hsv = cv2.merge([h, s, v])

    #CONVERT BACK TO BGR
    adjusted_frame = cv2.cvtColor(adjusted_hsv, cv2.COLOR_HSV2BGR)

    #DISPLAY THE ORIGINAL FRAME
    combined_frame = np.hstack((frame, adjusted_frame))
    #DISPLAY ADJUSTED FRAME
    cv2.imshow('ORIGINAL VS GOLDEN HOUR FILTER', combined_frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video.release()
cv2.destroyAllWindows()