import cv2
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec

ImageA_RGB = cv2.imread('Images/smoothies.jpg', cv2.IMREAD_COLOR)
ImageB_RGB = cv2.imread('Images/mario.jpg', cv2.IMREAD_COLOR)

if ImageA_RGB is None or ImageB_RGB is None:
    print("Error: Could not read one or both images")
    exit()

#CONVERT TO GRAYSCALE
ImageA1 = cv2.cvtColor(ImageA_RGB, cv2.COLOR_BGR2GRAY)
ImageB1 = cv2.cvtColor (ImageB_RGB, cv2.COLOR_BGR2GRAY)

#RETRIEVE SIZES OF IMAGE
sizeA = ImageA1.shape
sizeB = ImageB1.shape

if sizeA != sizeB:
    #RETRIEVE BASED ON WIDTH AND HEIGHT OF IMAGEA1
    print("The images are different sizes. Resizing ImageB1 to match ImageA1.")
    ImageB1 = cv2.resize(ImageB1, (sizeA[0], sizeA[1]))

else:
    print("The images are the same size, therefore I can continue!")

#THRESHOLD THE GRAYSCALE IMAGES TO CREATE A BINARY RESULT
#127 IS THE THRESHOLD VALUE. CAN BE ADJUSTED
_, ImageA2 = cv2.threshold(ImageA1, 127, 255, cv2.THRESH_BINARY)
_, ImageB2 = cv2.threshold(ImageB1, 127, 255, cv2.THRESH_BINARY)

ImageC = cv2.bitwise_and(ImageA2, ImageB2)

fig = plt.figure()
gs = gridspec.GridSpec(2, 5, width_ratios=[1, 1, 1, 2, 2], height_ratios=[2,1])

ax1 = plt.subplot(gs[0])
ax1.imshow(cv2.cvtColor(ImageA_RGB, cv2.COLOR_BGR2RGB))
ax1.set_title('ORIGINAL A')
ax1.axis('off')

ax2 = plt.subplot(gs[1])
ax2.imshow(ImageA1, cmap='gray')
ax2.set_title('GRAYSCALE')
ax2.axis('off')

ax3 = plt.subplot(gs[2])
ax3.imshow(ImageA2, cmap='gray')
ax3.set_title('BINARY')
ax3.axis('off')

ax4 = plt.subplot(gs[5])
ax4.imshow(cv2.cvtColor(ImageB_RGB, cv2.COLOR_BGR2RGB))
ax4.set_title('ORIGINAL B')
ax4.axis('off')

ax5 = plt.subplot(gs[6])
ax5.imshow(ImageB1, cmap='gray')
ax5.set_title('GRAYSCALE')
ax5.axis('off')

ax6 = plt.subplot(gs[7])
ax6.imshow(ImageB2, cmap='gray')
ax6.set_title('BW')
ax6.axis('off')

ax7 = plt.subplot(gs[3:5])
ax7.imshow(ImageC, cmap='gray')
ax7.set_title('AND IMAGE')
ax7.axis('off')

MyImageBw = np.copy(ImageA2)

Mystrel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(11,11))

MyDialation = cv2.dilate(MyImageBw, Mystrel, iterations=1)

MyErosion = cv2.erode(MyImageBw, Mystrel, iterations=1)


Images = [cv2.cvtColor(ImageA1, cv2.COLOR_BGR2RGB), MyImageBw, MyDialation, MyErosion, Mystrel]
titles = ['original', 'Bwimage', 'Dialation', 'Erosion', 'MyStrel']

plt.figure()

for i, (img,title) in enumerate(zip(Images,titles), 1):
    plt.subplot(1, 5, i)
    if i == 1:
        plt.imshow(img)
    else:
        plt.imshow(img, cmap='gray')
    plt.title(title)
    plt.axis("off")


plt.tight_layout()
plt.show()
