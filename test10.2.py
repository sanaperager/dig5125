import cv2
import numpy as np

foreground_video = cv2.VideoCapture('Videos/Flamingo.mp4')
background_video = cv2.VideoCapture('Videos/NatureVideo.mp4')

if not foreground_video.isOpened() or not background_video.isOpened():
    print("Error: COULD NOT OPEN ONE OR BOTH VIDEOS.")
    exit()

while True:
    ret_fg, frame_fg = foreground_video.read()
    ret_bg, frame_bg = background_video.read()

    if not ret_fg or not ret_bg:
        break

    hsv = cv2.cvtColor(frame_fg, cv2.COLOR_BGR2HSV)

    lower_green = np.array([40, 40, 40])
    upper_green = np.array([70, 255, 255])

    mask = cv2.inRange(hsv, lower_green ,upper_green)
    mask_inv = cv2.bitwise_not(mask)

    fg = cv2.bitwise_and(frame_fg , frame_fg , mask = mask_inv)

    frame_bg = cv2.resize(frame_bg , ( fg . shape[1] ,fg.shape[0]))

    bg = cv2.bitwise_and(frame_bg ,frame_bg , mask=mask)

    combined = cv2.add(fg , bg)

    cv2.imshow('copy=Chroma Keying', combined)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
       
foreground_video.release()
background_video.release()
cv2.destroyAllWindows()