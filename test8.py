import cv2

cap = cv2.VideoCapture('videos/video1.avi')

if not cap.isOpened():
    print ("ERROR: Could not open video")
    exit()
   
   
while cap.isOpened():
    ret, frame = cap.read()
    if ret:
        cv2.imshow('Frame', frame)
       
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
       
    else:
        break
   
cap.release()
cv2.destroyAllWindows()
