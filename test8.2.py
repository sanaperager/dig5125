import cv2

#START VIDEO CAPTURE
cap = cv2.VideoCapture(0)

#CHECK IF WEBCAM IS OPEN
if not cap.isOpened():
    raise IOError("CANNOT OPEN WEBCAM")

while True:
    ret, frame = cap.read()
    if not ret:
        break

    cv2.imshow('Webcam Live', frame)

#BREAK LOOP WHEN Q IS PRESSED
    if cv2.waitKey(1) & 0xFF == ord('q'):
         break

#RELEASE CAPTURE
cap.release()
cv2.destroyAllWindows()
