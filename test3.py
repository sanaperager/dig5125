import cv2
import matplotlib.pyplot as plt
import numpy as np

#READING GRAYSCALE IMAGE
image = cv2.imread('Images/saturn.png', 0)

#CALCULATE HISTOGRAM
hist = cv2.calcHist([image], [0], None, [256], [0, 256])

#X VALUES
x_values = np.arange(256)

plt.subplot(1,2,1)
plt.imshow(image, cmap='gray')
plt.axis('off')
plt.title('ORIGINAL IMAGE')

plt.subplot(1,2,2)
plt.bar(x_values, hist.ravel(), color= 'gray')
plt.title("GRAYSCALE HISTOGRAM")
plt.xlabel("PIXEL VALUE")
plt.ylabel("FREQUENCY")
plt.tight_layout()
plt.show()
