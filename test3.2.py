import cv2
import matplotlib.pyplot as plt
import numpy as np

image = cv2.imread('Images/smoothies.jpg')
#BGR TO RGB
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

#HISTOGRAMS
hist = cv2.calcHist([image], [0], None, [256], [0, 256])
hist_r = cv2.calcHist([image], [0], None, [256], [0, 256])
hist_g = cv2.calcHist([image], [1], None, [256], [0, 256])
hist_b = cv2.calcHist([image], [2], None, [256], [0, 256])

x_values = np.arange(256)

#PLOTTING
plt.figure
plt.bar(x_values, hist_r.ravel(), color='red', alpha=0.5, label='RED CHANNEL')
plt.bar(x_values, hist_g.ravel(), color='green', alpha=0.5, label='GREEN CHANNEL')
plt.bar(x_values, hist_b.ravel(), color='blue', alpha=0.5, label='BLUE CHANNEL')
plt.title("RGB HISTOGRAM")
plt.xlabel("PIXEL VALUE")
plt.ylabel("FREQUENCY")
plt.legend()
plt.show()

plt.subplot(2,2,1)
plt.bar(x_values, hist.ravel(), color= 'gray')
plt.title("GRAYSCALE HISTOGRAM")
plt.xlabel("PIXEL VALUE")
plt.ylabel("FREQUENCY")

plt.subplot(2,2,2)
plt.bar(x_values, hist_r.ravel(), color= 'red')
plt.title("RED HISTOGRAM")
plt.xlabel("PIXEL VALUE")
plt.ylabel("FREQUENCY")

plt.subplot(2,2,3)
plt.bar(x_values, hist_g.ravel(), color= 'green')
plt.title("GREEN HISTOGRAM")
plt.xlabel("PIXEL VALUE")
plt.ylabel("FREQUENCY")

plt.subplot(2,2,4)
plt.bar(x_values, hist_b.ravel(), color= 'blue')
plt.title("BLUE HISTOGRAM")
plt.xlabel("PIXEL VALUE")
plt.ylabel("FREQUENCY")
plt.tight_layout()
plt.show()