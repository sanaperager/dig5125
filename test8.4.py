import cv2

cap = cv2.VideoCapture('Videos/Video1.avi')

if not cap.isOpened():
    print("Error: Could not open video.")
    exit()

width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
height = cap.get(cv2. CAP_PROP_FRAME_HEIGHT)
fps = cap.get(cv2.CAP_PROP_FPS)
frame_count = cap.get(cv2.CAP_PROP_FRAME_COUNT)
codec_code = int(cap.get(cv2.CAP_PROP_FOURCC))
codec = "".join([chr((codec_code >> 8 * i) & 0xFF) for i in range(4)])

print(f"Width: {width}")
print(f"Height: {height}")
print(f"FPS: {fps}")
print(f"Numbver of frames : {frame_count}")
print(f"Codec: {codec}")

cap.release()
