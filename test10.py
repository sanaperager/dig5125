import cv2
import numpy as np
import time

#INITIALISE VIDEO CAPTURE OBJECT

cap = cv2.VideoCapture('Videos/Cars.mp4')

#READ FIRST FRAME
ret, prev_frame = cap.read()
if not ret:
    print ("FAILED TO READ THE VIDEO")
    cap.release()
    exit()

#CONVERT FIRST FRAME TO GRAYSCALE
prev_frame = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)

#FPS
fps = cap . get ( cv2 . CAP_PROP_FPS )
frame_interval = 1.0 / fps

#LOOP OVER ALL FRAMES IN VIDEO
while True:
    start_time = time.time()
    #READ NEW FRAME
    ret, frame = cap.read()
    if not ret:
        break
    ret, curr_frame = cap.read()
    if not ret:
        break
    #CONVERT TO GRAYSCALE AND APPLY GAUSSIAN BLUR
    curr_frame_gray = cv2.cvtColor(curr_frame, cv2.COLOR_BGR2GRAY)
    #FRAME DIFFERENCING
    frame_diff = cv2.absdiff(prev_frame, curr_frame_gray)
    #DISPLAY RESULT
    cv2.imshow('FRAME DIFFERENCE', frame_diff)
    prev_frame = curr_frame_gray

    elapsed_time = time.time()-start_time
    delay = max(int((frame_interval - elapsed_time)*10000), 1)

    #BREAK LOOP IF 'q' IS TRIGGERED
    if cv2.waitKey(delay) & 0xFF == ord('q'):
        break

#RELEASE THE VIDEO CAPTURE OBJECT AND CLOSE ALL WINDOWS
cap.release()
cv2.destroyAllWindows()