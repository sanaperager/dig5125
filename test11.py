import cv2
import numpy as np
import time

cap = cv2.VideoCapture(0)
ret, prev_frame = cap.read()

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    #FRAME DIFFERENCING
    frame_diff = cv2.absdiff(prev_frame,frame)
    #DISPLAY RESULT
    cv2.imshow('FRAME DIFFERENCE', frame_diff)
    prev_frame = frame.copy()

    #BREAK LOOP IF 'q' IS TRIGGERED
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

#RELEASE THE VIDEO CAPTURE OBJECT AND CLOSE ALL WINDOWS
cap.release()
cv2.destroyAllWindows()