import cv2
import numpy as np
import matplotlib.pyplot as plt

#READ THE IMAGE INTO VARIABLE I
image = cv2.imread('Images/googlecat.jpg')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) 
#IMAGE DIMENSIONS
print(np.shape(image))
i=np.copy(image)
#ACCESS A SINGLE PIXEL
pixel_value = i[50, 50]
#ACCESS A ROW
row_values = i[50, :]
#ACCESS A CHANNEL
red_channel = i[:, :, 0]

#INCREASE THE INTENSITY OF THE BLUE CHANNEL BY 30
i[:, :, 2] += 30
#INCREASE THE INTENSITY OF THE GREEN CHANNEL BY 60
i[:,:,1] += 60


plt.subplot(1,2,2)
plt.imshow(cv2.cvtColor(i, cv2.COLOR_BGR2RGB))
plt.axis('off')
plt.title('THRESHOLDED IMAGE')

plt.show()


