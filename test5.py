from scipy.signal import convolve2d
import numpy as np
import cv2 
import math

def simple_1d_convolution(Input_array, conv_mask):
    return np.convolve(Input_array, conv_mask, mode='full')

input_array = np.array([1, 2, 3, 4, 5, 6])
conv_mask = np.array([1, 1, 1, 1, 1])
convolution_result = simple_1d_convolution(input_array, conv_mask)
print("CONVOLUTION RESULT:", convolution_result)

