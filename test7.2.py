import cv2
import numpy as np

#LOADING IMAGE
image = cv2.imread('Images/kitten.jpg', cv2.IMREAD_GRAYSCALE)

#DEFINING SOBEL KERNALS HORIZONTALLY (X) AND VERTICALLY (Y) AND THEIR SIZE
sobel_x = cv2.Sobel(image, cv2.CV_64F, 1, 0, ksize=3)
sobel_y = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=3)

#COMBINING THE DIFFERENT KERNALS AND CLIPPING IT
edge_magnitude = np.sqrt(np.square(sobel_x) + np.square(sobel_y))
edge_magnitude = np.clip(edge_magnitude, 0, 255)
edge_magnitude = np.uint8(edge_magnitude)

#APPLYING AND DISPLAYING SOBEL
cv2.imshow('ORIGINAL', image)
cv2.imshow('SOBEL X', sobel_x)
cv2.imshow('SOBEL Y', sobel_y)
cv2.imshow('EDGE MAGNITUDE', edge_magnitude)
cv2.waitKey(0)
cv2.destroyAllWindows()