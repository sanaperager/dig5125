import cv2

#HAAR CASCADE MODEL FOR FACE DETECTION
face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

#INITALISE VIDEO CAPTURE/WEBCAM
cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    if not ret:
        break

    #GRAYSCALE
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #DETECT FACES
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    for (x,y,w,h) in faces:
        face = frame[y:y + h, x:x + w]

        #GAUSSIAN BLUR
        face = cv2.GaussianBlur(face, (99, 99), 30)
        #REPLACE FACE IN ORIGINAL FRAME WITH BLURRED FACE
        frame[y:y + h, x:x + w] = face

    #DISPLAY
    cv2.imshow('FACE DETECTION AND BLUR', frame)
    if cv2.waitKey(1) * 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()