import cv2

#HAAR CASCADE MODEL FOR FACE DETECTION
face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

#INITALISE VIDEO CAPTURE/WEBCAM
cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    if not ret:
        break

    #GRAYSCALE
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #DETECT FACES
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    #DRAW RECTANGLES AROUND THE FACE
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x,y), (x+w, y+h), (255, 0, 0), 2)

        #DISPLAY OUTPUT
        cv2.imshow('FACE TRACKING', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()