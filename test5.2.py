from scipy.signal import convolve2d
import numpy as np
import cv2 
import math

#HORIZONTAL AND VERTICAL KERNAL
conv_kernal1 = np.array([[1,1,1], [0,0,0],[-1,-1,-1]])
conv_kernal2 = np.array([[-1,0,1], [-1,0,1], [-1,0,1]])
print(conv_kernal1)

image = cv2.imread('Images/smoothies.jpg', cv2.IMREAD_GRAYSCALE)
#BLURRING IMAGE. ADJUSTING NUMBERS ADJUSTS THE AMOUNT OF BLURRING
image = cv2.blur (image, (11,11))
#PLOTTING
cv2.imshow('grayscale', image)
cv2.waitKey(0)
cv2.destroyAllWindows()

output_x = convolve2d(image, conv_kernal1, mode='full')**2
output_y = convolve2d(image, conv_kernal2, mode='full')**2
processed_image = (output_x + output_y)**1/2
#EVERYTHING WITHIN THE CORRECT RANGE
processed_image = np.clip(processed_image, 0, 255)
processed_image = processed_image.astype('uint8')

cv2.imshow('output', processed_image)
cv2.waitKey(0)
cv2.destroyAllWindows()