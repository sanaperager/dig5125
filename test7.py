import cv2
import numpy as np
import matplotlib.pyplot as plt

#LOADING IMAGE
image = cv2.imread('Images/kitten.jpg', cv2.IMREAD_GRAYSCALE)

#DEFINING ROBERTS OPERATOR KERNALS
roberts_x = np.array([[1,0], [0, -1]], dtype=np.float32)
roberts_y = np.array([[0,1], [-1, -0]], dtype=np.float32)

#APPLYING ROBERTS OPERATOR
edge_x = cv2.filter2D(image, -1, roberts_x)
edge_y = cv2.filter2D(image, -1, roberts_y)

#CALCULATING EDGE MAGNITUDE
edge_magnitude = np.sqrt(np.square(edge_x) + np.square(edge_y))
edge_magnitude = np.uint8(edge_magnitude)

#DISPLAYING RESULTS
plt.subplot(121), plt.imshow(image, cmap='gray'), plt.title('ORIGINAL IMAGE')
plt.subplot(122), plt.imshow(edge_magnitude, cmap='gray'), plt.title('EDGE IMAGE (Roberts Operator)')
plt.show()
